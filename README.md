# Theme engine examples for EmulationStation Desktop Edition (theme-engine-examples-es-de)

This is an example theme set intended for theme developers to learn how to use various ES-DE theme engine capabilities.

The theme engine documentation can be found here:

https://gitlab.com/es-de/emulationstation-de/-/blob/master/THEMES.md

https://gitlab.com/es-de/emulationstation-de/-/blob/master/THEMES-DEV.md

The following options are included:

6 variants:

- 01 - Horizontal carousel and textlist
- 02 - System view game info and wheel carousels
- 03 - Textlist and vertical carousel
- 04 - Textlist and carousel with reflections
- 05 - System and gamelist grids
- 06 - GIF and Lottie animations

2 color schemes:

- Dark mode
- Light mode

2 aspect ratios:

- 16:9
- 4:3

# Credits

The theme is based on [recalbox-multi](https://gitlab.com/recalbox/recalbox-themes) by the Recalbox community.
